import pandas as pd
import numpy as np
import clickhouse_connect
from clickhouse_driver import Client
import fuzzywuzzy
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from pyaspeller import YandexSpeller
import dask.dataframe as dd
from collections import defaultdict
from aiogram import Bot, Dispatcher, executor, types
import asyncio
import psycopg2
from sqlalchemy import create_engine


APITOKEN = '5804874123:AAHKWhnLonqugKNi6oMpQ7e8X1zYy-Ccm4g'
DEFAULT_MSG = 'Привет, { USERNAME }!'
bot = Bot(APITOKEN)
dispatcher = Dispatcher(bot=bot)


# ----------------------------------------------------------------
# create dict
# ----------------------------------------------------------------


def load_df(sql): 
    result, columns = client.execute(sql, with_column_types=True)
    df = pd.DataFrame(result, columns=[name[0] for name in columns])
    return df
user = 'chusova_m'
password = '53nK8k0KdqgCG0sV'
client = Client('clickhouse.ga.loc', user=user, password=password)


sql = f"""

select distinct campaign_name from dwh.sku_price_action 
where action_dbeg >= '2021-01-01' and (action_type_id = 16) or (action_type_id = 12) or (action_type_id = 36) or (action_type_id = 23) or (action_type_id = 27) or (action_type_id = 26)

"""
df = load_df(sql)
g16 = list(df['campaign_name'])

dc0 = {'выделенный ассортимент':'reaction',
'скидка на выделенный асс':'reaction',
'выделанный ассортимент':'reaction',
'Скидка на выделенный асс -2':'reaction',
'Скидка по модели':'reaction',
'Реакция': 'reaction',
'День Святого Валентина':'gender',
'День всех влюбленных':'gender',
'Мужской ассортимент до':'gender',
'Спец':' special ',
'Скидка со спецов':' special ',
'Распродажа':'discount',
'Скидки на весь ассортимент':'discount',
'Скидки на ПК':'discount',
'Вальмон, Тальго, Др.Джарт, Озу, Мимианг, Сорри, Леонор Грейл, Этро, Корин де Фарм 30%':'discount',
'Др.Джарт 35%':'discount',
'21.01-24.01 Азиатский новый год':'discount',
'11.11 Скидка на БАДы':'discount',
'Скидка на спецпредложения':'special',
'ДР':'hb',
'День рождения':'hb',
'Открытие': 'hb',
'Киберпонедельник':'cybermon',
'Черная пятница':'blackfriday',
'НГ':'ny',
'Новый год':'ny',
'Сет':'chains',
'Сетевые':'chains',
'Сеть':'chains',
'Halloween':'hw',
'Хэллоуин':'hw',
'Хеллоуин':'hw',
'Клиентский день':'cd',
'Клиентские дни':'cd',
'КД':'cd',
'КД Molto Brown и др. 25%':'cd'
}

dc = {'ДР',
'НГ',
'Новый год',
'Сет',
'Сетевые',
'Сеть',
'КД',
'Реакция',
'Клиентский день',
'Клиентские дни',
'День рождения'
'Спец',
'Скидка на спецпредложения',
'Скидка со спецов',
'Черная пятница',
'Киберпонедельник',
'Хэллоуин',
'Halloween',
'День защитника отечества',
'8 марта',
'Международный женский день',
'День Святого Валентина',
'День всех влюбленных',
'скидка',
'Распродажа',
'выделенный ассортимент',
'Скидка по модели',
'Скидки на весь ассортимент',
'выделанный ассортимент',
'скидка на выделенный асс',
'Скидка на выделенный асс -2',
'Скидки на ПК',
'Мужской ассортимент до',
'Хеллоуин',
'Открытие'
}

stores = {'169':170,
'175':174,
'160':161,
'187':186,
'185':184,
'135':134,
'173':172,
'803':122,
'127':126,
'189':188,
'364':164,
'177':176,
'901':900,
'198':197,
'191':190,
'129':128,
'103':111,
'181':180,
'119':111,
'104':134,
'142':141,
'179':178,
'165':166,
'167':168,
'154':153,
'404':403,
'123':122,
'156':155,
'118':108,
'802':126,
'120':110,
'105':128,
'196':195,
'117':107,
'106':122,
'163':164,
'167':168,
'116':115}

def sortFunc(param):
    return param[2]

arrayAllLooksLike = []
for item in (g16):
    itemOne = item
    itemArray = []
    
    for itemDC in dc0:
        lookLike = int(f'{fuzz.token_set_ratio(itemOne, itemDC)}')
        lookLikeArray = []
        
        lookLikeArray.append(itemOne)
        lookLikeArray.append(itemDC)
        lookLikeArray.append(lookLike)
        
        if ( lookLike >= 67 ):
            
            itemArray.append(lookLikeArray)
            
        # print('---------------------')
        # print(itemOne)
        # print(itemDC)
        # print(lookLike)
        # print('---------------------')

    # print(sorted(itemArray, key=sortFunc, reverse=True))
    # print('---------------------')

    sordedItemArray = sorted(itemArray, key=sortFunc, reverse=True)
    
    if ( bool(sordedItemArray) ):
        
        arrayAllLooksLike.append(sordedItemArray[0])
        
        # print('---------------------')
        # print(sordedItemArray[0])
        # print(bool(sordedItemArray))
        # print('---------------------')

dc3 = {}
list1 = []
list2 = []
for item in arrayAllLooksLike:
    list1.append(item[0])

for item in arrayAllLooksLike:
    list2.append(item[1])

for i in range (len(list1)):
    dc3[list1[i]] = list2[i]


arrayAllLooksLike = pd.DataFrame(arrayAllLooksLike)

arrayAllLooksLike.to_excel('./array.xlsx') # получаем данные по campaign_name до обработки и после


# ----------------------------------------------------------------
# rec campaign_name in df
# ----------------------------------------------------------------


def load_ax(sql): 
    result, columns = client.execute(sql, with_column_types=True)
    df = pd.DataFrame(result, columns=[name[0] for name in columns])
    return df
user = 'chusova_m'
password = '53nK8k0KdqgCG0sV'
client = Client('clickhouse.ga.loc', user=user, password=password)
sql = f"""

select reportDate, itemid, barcode, brend_name, store_number, campaign_name, action_name, action_type_id, action_dbeg, action_dend, price, itog_rate, ver
from dwh.sku_price_action 
where action_dbeg >= '2021-01-01' and (action_type_id = 16) or (action_type_id = 12) or (action_type_id = 36) or (action_type_id = 23) or (action_type_id = 27)
limit 1000000
"""
ax = load_ax(sql)

df = (ax
      .query("campaign_name in @dc3.keys()")
      .assign(campaign_name=lambda x: x["campaign_name"].replace(dc3)))

ax.to_excel('./ax.xlsx')

df = (df
      .query("campaign_name in @dc0.keys()")
      .assign(campaign_name=lambda x: x["campaign_name"].replace(dc0)))

df.to_excel('./df.xlsx')

df = (df
      .query("store_number in @stores.keys()")
      .assign(store_number=lambda x: x["store_number"].replace(stores)))
df.to_excel('./df.xlsx')

sortValues = []


# ----------------------------------------------------------------
# отправка аутпута из нечеткого поиска с размерностью меньше 67 в тг
# ----------------------------------------------------------------


sortValues = []
list1 = list(arrayAllLooksLike[0])

for item in arrayAllLooksLike[0]:
    sortValues.append(item)


print(sortValues)
diff = pd.DataFrame(set(g16).difference(set(sortValues)))

diff.to_excel('./diff.xlsx')




# import pandas as pd
# info = pd.DataFrame({'Language known': ['Python', 'Android', 'C', 'Android', 'Python', 'C++', 'C']},
# index=['Parker', 'Smith', 'John', 'William', 'Dean', 'Christina', 'Cornelia'])
# print(info)
# dictionary = {"Python": 1, "Android": 2, "C": 3, "Android": 4, "C++": 5}
# info1 = info.replace({"Language known": dictionary})
# print("\n\n")
# print(info1)


# ----------------------------------------------------------------
# check results in tg
# ----------------------------------------------------------------


# если инпут = аутпут, то ок
# выводить  diff.xlsx
# выводить текстом новые данные для словаря


# @dispatcher.message_handler(commands=["start"])
# async def start_handler(message: types.Message):
    
#     USERID = message.from_user.id
#     USERNAME = message.from_user.full_name

#     await message.reply(f"Привет, { USERNAME }!")

#     for i in range(7):
#       await asyncio.sleep(60*60*24)
#       await bot.send_message(USERID, DEFAULT_MSG.format(USERNAME))
    
#     for i in diff:
#         bot.send_message(i)
      
# if __name__ == "__main__":
#     executor.start_polling(dispatcher)


# ----------------------------------------------------------------
# rec to postgres
# ----------------------------------------------------------------


# connection = psycopg2.connect(user="chusova_m",
#                                 password="KKaIdvAF#TSGYn&q",
#                                 host="etl-postgres-01",
#                                 port="5432",
#                                 database="postgres")

# cursor = connection.cursor()
# schema = 'analytics'
# engine = create_engine('postgresql+psycopg2://chusova_m:KKaIdvAF#TSGYn&q@etl-postgres-01:5432/postgres')

# df.to_sql('test',
#             schema = schema,
#             con = engine,
#             #   if_exists= 'replace',
#             #   if_exists = 'append',
#             index = False)