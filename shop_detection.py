import pandas as pd
import numpy as np
import clickhouse_connect
from clickhouse_driver import Client
import fuzzywuzzy
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from pyaspeller import YandexSpeller

def load_df(sql): 
    result, columns = client.execute(sql, with_column_types=True)
    df = pd.DataFrame(result, columns=[name[0] for name in columns])
    return df
user = 'chusova_m'
password = '53nK8k0KdqgCG0sV'
client = Client('clickhouse.ga.loc', user=user, password=password)

sql = f"""
SELECT *
    FROM
      (SELECT DISTINCT 
         DATAAREAID 
        ,SALESID
        ,BRO_MULTIORDERID AS n_zakaza
        ,toDate(CREATEDDATETIME) AS order_data
        ,BRO_ISMULTIPARCELORDER
        ,SUM(SALESQTY)/1000 AS QTY
        ,SUM(LINEAMOUNT)/100 AS revenue
        ,DELIVERYNAME
        ,DELIVERYPOSTALADDRESS as post_adres
        ,RECEIPTDATEREQUESTED
        ,DLVMODE as deliverymethod
        ,INVENTSITEID as stock 
      FROM dwh.ax_salestable as2 
      WHERE order_data >='2023-04-01'
      AND order_data <='2023-04-01'
      and DATAAREAID not like ('ggr')
      and ifNull(deliverymethod, '') != ''
      GROUP  BY DATAAREAID ,SALESID,n_zakaza,order_data,deliverymethod ,stock ,BRO_ISMULTIPARCELORDER,DELIVERYNAME,post_adres,RECEIPTDATEREQUESTED   
      HAVING sum(Sign)>0
    )LEFT JOIN
      (SELECT DISTINCT toInt64(RecId) AS post_adres, County,CityDescription AS city,GDA_DeliveryDistrict AS District
      FROM dwh.ax_LogisticsPostalAddress alpa 
      )USING(post_adres)

"""
df = load_df(sql)

result = []
for item in df['County']: 
    if item not in result: 
        result.append(item)

# print(type(result))

dc = {"Алтайский край",
"Амурская область",
"Архангельская область",
"Астраханская область",
"Белгородская область",
"Брянская область",
"Владимирская область ",
"Волгоградская область",
"Вологодская область",
"Воронежская область",
"Еврейская автономная область",
"Забайкальский край",
"Ивановская область",
"Иркутская область",
"Кабардино-Балкарская Республика",
"Калининградская область",
"Калужская область",
"Камчатский край",
"Карачаево-Черкесская Республика",
"Кемеровская область",
"Кировская область",
"Костромская область",
"Краснодарский край",
"Красноярский край",
"Курганская область",
"Курская область",
"Ленинградская область",
"Липецкая область",
"Магаданская область",
"Москва",
"Московская область",
"Мурманская область",
"Ненецкий АО",
"Нижегородская область",
"Новгородская область",
"Новосибирская область",
"Омская область",
"Оренбургская область",
"Орловская область",
"Пензенская область",
"Пермский край",
"Приморский край",
"Псковская область",
"Республика Адыгея",
"Республика Алтай",
"Республика Башкортостан",
"Республика Бурятия",
"Республика Дагестан",
"Республика Ингушетия",
"Республика Калмыкия",
"Республика Карелия",
"Республика Коми",
"Республика Крым",
"Республика Марий Эл",
"Республика Мордовия",
"Республика Саха (Якутия)",
"Республика Северная Осетия - Алания",
"Республика Татарстан",
"Республика Тыва",
"Республика Хакасия",
"Ростовская область",
"Рязанская область",
"Самарская область",
"Санкт-Петербург",
"Саратовская область",
"Сахалинская область",
"Свердловская область",
"Севастополь",
"Смоленская область",
"Ставропольский край",
"Тамбовская область",
"Тверская область",
"Томская область",
"Тульская область",
"Тюменская область",
"Удмуртская Республика",
"Ульяновская область",
"Хабаровский край",
"Ханты-Мансийский Автономный округ - Югра",
"Челябинская область",
"Чеченская Республика",
"Чувашская Республика",
"Чукотский автономный округ",
"Ямало-Ненецкий",
"Ярославская область",
"Брестская область",
"Витебская область",
"Гомельская область",
"Гродненская область",
"Минская область",
"Могилёвская область",
"Абайская область",
"Акмолинская область",
"Актюбинская область",
"Алматинская область",
"Атырауская область",
"Восточно-Казахстанская область",
"Жамбылская область",
"Жетысуская область",
"Западно-Казахстанская область",
"Карагандинская область",
"Костанайская область",
"Кызылординская область",
"Мангистауская область",
"Павлодарская область",
"Северо-Казахстанская область",
"Туркестанская область",
"Улытауская область",
"Южно-Казастанская",
"Минск",
"Беларусь",
"Нур-Султан",
"Алматы",
"Шымкент"}

# НЕЧЕТКИЙ ПОИСК ОБЛАСТИ (partial ratio)

def sortFunc(param):
    return param[2]

arrayAllLooksLike = []
arraydist = []
arrayright = []
arrayAllLooksLikeFalse = []

for item in (list(result)):
    itemOne = item
    itemArray = []
    
    for itemDC in dc:
        lookLike = int(f'{fuzz.ratio(itemOne, itemDC)}')
        lookLikeArray = []
        
        lookLikeArray.append(itemOne)
        lookLikeArray.append(itemDC)
        lookLikeArray.append(lookLike)
        
        if ( lookLike >= 86 ):
            
            itemArray.append(lookLikeArray)
            
        # print('---------------------')
        # print(itemOne)
        # print(itemDC)
        # print(lookLike)
        # print('---------------------')

    # print(sorted(itemArray, key=sortFunc, reverse=True))
    # print('---------------------')
    
    sordedItemArray = sorted(itemArray, key=sortFunc, reverse=True)
    
    if ( bool(sordedItemArray) ):
        
        arrayAllLooksLike.append(sordedItemArray[0])
        
        # print('---------------------')
        # print(sordedItemArray[0])
        # print(bool(sordedItemArray))
        # print('---------------------')

# print(arrayAllLooksLike)

for item in arrayAllLooksLike:
    arraydist.append(item[0])

for item in arrayAllLooksLike:
    arrayright.append(item[1])

dc2 = {}
for i in range (len(arraydist)):
    dc2[arraydist[i]] = arrayright[i]


#НЕЧЕТКИЙ ПОИСК ОСТАВШИХСЯ ОБЛАСТЕЙ (weighted ratio)

sortValues = []

for item in arrayAllLooksLike:
    sortValues.append(item[0])

# print(sortValues)
diff = list(set(result).difference(set(sortValues)))

def sortFunc2(param):
    return param[2]
list1 = []
list2 = []
arrayDiffLike = []

for item in list(diff):
    itemOne = item
    itemArray = []
    
    for itemDC in dc:
        diffLike = int(f'{fuzz.token_set_ratio(itemOne, itemDC)}')
        diffArray = []
        # print('---------------------')
        # print(itemOne)
        # print(itemDC)
        # print(diffLike)
        # print('---------------------')

        diffArray.append(itemOne)
        diffArray.append(itemDC)
        diffArray.append(diffLike)
        
        if ( diffLike >= 75 ):
            
            itemArray.append(diffArray)
            
        # print('---------------------')
        # print(itemOne)
        # print(itemDC)
        # print(diffLike)
        # print('---------------------')

# print(sorted(itemArray, key=sortFunc2, reverse=True))
# print('---------------------')

    sordedItemArray = sorted(itemArray, key=sortFunc2, reverse=True)

    if ( bool(sordedItemArray) ):
        
        arrayDiffLike.append(sordedItemArray[0])
        
        # print('---------------------')
        # print(sordedItemArray[0])
        # print(bool(sordedItemArray))
        # print('---------------------')

dc3 = {}

for item in arrayDiffLike:
    list1.append(item[0])

for item in arrayDiffLike:
    list2.append(item[1])

for i in range (len(list1)):
    dc3[list1[i]] = list2[i]

yyy = dc2 | dc3

result = (df
      .query("County in @yyy.keys()")
      .assign(County=lambda x: x["County"].replace(yyy)))


# ----------------------------------------------------------------
# telegram
# ----------------------------------------------------------------


# ----------------------------------------------------------------
# создание словаря способов доставки
# ----------------------------------------------------------------

dm = dict(store='Курьер',
TRACK='Курьер',
двери='Курьер',
ПочтаРФ='СамовывозД',
Курьер='Курьер',
from_store='Курьер',
Куьер='Курьер',
Доставка='Курьер',
Самовывоз='СамовывозД',
ПочтаРФПос='СамовывозД',
СамовывозД='СамовывозД',
ДоставкаЗЯ='Курьер',
СамовывозЗ='СамовывозЗ',
КурьерМарш='Курьер',
БескКурьер='Курьер')

df = (result
      .query("deliverymethod in @dm.keys()")
      .assign(deliverymethod=lambda x: x["deliverymethod"].replace(dm)))

# ----------------------------------------------------------------
# выгрузка матрицы приоритетов
# ----------------------------------------------------------------

def load_ax(sql): 
    result, columns = client.execute(sql, with_column_types=True)
    df = pd.DataFrame(result, columns=[name[0] for name in columns])
    return df
user = 'chusova_m'
password = '53nK8k0KdqgCG0sV'
client = Client('clickhouse.ga.loc', user=user, password=password)
sql = f"""
SELECT INVENTLOCATIONID ,LOGISTICSADDRESSCITY_NAME as County ,LOGISTICSADDRESSCITY_DESCRIPTION as city ,GDA_DELIVERYRATES_DLVMODEID as deliverymethod, DELIVERYTIME ,RATEVALUE1KG , CALENDARID , argMax(ver,ver)
,multiIf(DELIVERYSERVICEID='Боксберри Софт','10'
              ,DELIVERYSERVICEID='DPD','16'
              ,DELIVERYSERVICEID='Logsis','10'
              ,DELIVERYSERVICEID='Далли','10'
              ,DELIVERYSERVICEID='5Post','10'
              ,DELIVERYSERVICEID='Золотое Яблоко','3'
              ,DELIVERYSERVICEID='Золотое Яблоко Адлер','1'
              ,DELIVERYSERVICEID='Золотое Яблоко Афи','1'
              ,DELIVERYSERVICEID='Золотое Яблоко АфиS','1'
              ,DELIVERYSERVICEID='Золотое Яблоко Дор','2'
              ,DELIVERYSERVICEID='Золотое Яблоко ДорS','1'
              ,DELIVERYSERVICEID='Золотое Яблоко Евро','1'
              ,DELIVERYSERVICEID='Золотое Яблоко КунS','1'
              ,DELIVERYSERVICEID='Золотое Яблоко Кунц','1'
              ,DELIVERYSERVICEID='Золотое Яблоко МетS','1'
              ,DELIVERYSERVICEID='Золотое Яблоко Нег','1'
              ,DELIVERYSERVICEID='Золотое Яблоко Пав','1'
              ,DELIVERYSERVICEID='Золотое Яблоко СалS','1'
              ,DELIVERYSERVICEID='Золотое Яблоко Салар','1'
              ,DELIVERYSERVICEID='Золотое Яблоко СПб','1'
              ,DELIVERYSERVICEID='Золотое Яблоко СПбN','1'
              ,DELIVERYSERVICEID='Золотое Яблоко СПбS','1'
              ,DELIVERYSERVICEID='ПикПоинт','15'
              ,DELIVERYSERVICEID='Почта','16'
              ,DELIVERYSERVICEID='Сберлогистика','13'
              ,DELIVERYSERVICEID='СДЭК','15'
              ,DELIVERYSERVICEID='ЯндексGo','5'
              ,DELIVERYSERVICEID='Казпочта','25'
              ,DELIVERYSERVICEID='Алем','25','25') AS service
from dwh.ax_delivery_cost_time_matrix adctm 
Where INVENTLOCATIONID NOT IN ('125')
 --and ENDDATE >= '1970-01-01'
  AND ENDDATE  NOT BETWEEN '2020-01-01' AND '2023-04-20'
--and city  ='Белгород'
--and deliverymethod ='Курьер'
--and DELIVERYSERVICEID ='5Post'
    AND  ver = (SELECT max(ver)
FROM dwh.ax_delivery_cost_time_matrix)
GROUP by INVENTLOCATIONID ,County ,city ,deliverymethod , DELIVERYSERVICEID , DELIVERYTIME ,RATEVALUE1KG,CALENDARID 
order by County ,city , deliverymethod ,DELIVERYTIME ,service ,RATEVALUE1KG  ASC 

"""
ax = load_ax(sql)
ax_res = []
for item in ax['County']: 
    if item not in ax_res: 
        ax_res.append(item)
        
        
# ----------------------------------------------------------------
# нечеткий поиск областей (partial_ratio)
# ----------------------------------------------------------------

def sortFunc(param):
    return param[2]

arrayAllLooksLike = []
arraydist = []
arrayright = []
arrayAllLooksLikeFalse = []

for item in (list(ax_res)):
    itemOne = item
    itemArray = []
    
    for itemDC in dc:
        lookLike = int(f'{fuzz.partial_ratio(itemOne, itemDC)}')
        lookLikeArray = []
        
        lookLikeArray.append(itemOne)
        lookLikeArray.append(itemDC)
        lookLikeArray.append(lookLike)
        
        if ( lookLike >= 100 ):
            
            itemArray.append(lookLikeArray)
            
        # print('---------------------')
        # print(itemOne)
        # print(itemDC)
        # print(lookLike)
        # print('---------------------')

# print(sorted(itemArray, key=sortFunc, reverse=True))
# print('---------------------')
    
    sordedItemArray = sorted(itemArray, key=sortFunc, reverse=True)
    
    if ( bool(sordedItemArray) ):
        
        arrayAllLooksLike.append(sordedItemArray[0])
        
        # print('---------------------')
        # print(sordedItemArray[0])
        # print(bool(sordedItemArray))
        # print('---------------------')

# print(arrayAllLooksLike)

for item in arrayAllLooksLike:
    arraydist.append(item[0])

for item in arrayAllLooksLike:
    arrayright.append(item[1])

dc2 = {}
for i in range (len(arraydist)):
    dc2[arraydist[i]] = arrayright[i]

# ----------------------------------------------------------------
# нечеткий поиск оставшихся областей (token_set_ratio)
# ----------------------------------------------------------------

sortValues = []

for item in arrayAllLooksLike:
    sortValues.append(item[0])

diff = list(set(ax_res).difference(set(sortValues)))

def sortFunc2(param):
    return param[2]
list1 = []
list2 = []
arrayDiffLike = []

for item in list(diff):
    itemOne = item
    itemArray = []
    
    for itemDC in dc:
        diffLike = int(f'{fuzz.token_set_ratio(itemOne, itemDC)}')
        diffArray = []
        # print('---------------------')
        # print(itemOne)
        # print(itemDC)
        # print(diffLike)
        # print('---------------------')

        diffArray.append(itemOne)
        diffArray.append(itemDC)
        diffArray.append(diffLike)
        
        if ( diffLike >= 50 ):
            
            itemArray.append(diffArray)
            

    # print(sorted(itemArray, key=sortFunc2, reverse=True))
    # print('---------------------')

    sordedItemArray = sorted(itemArray, key=sortFunc2, reverse=True)

    if ( bool(sordedItemArray) ):
        
        arrayDiffLike.append(sordedItemArray[0])
        
        # print('---------------------')
        # print(sordedItemArray[0])
        # print(bool(sordedItemArray))
        # print('---------------------')

# print(arrayDiffLike)

dc3 = {}

for item in arrayDiffLike:
    list1.append(item[0])

for item in arrayDiffLike:
    list2.append(item[1])

for i in range (len(list1)):
    dc3[list1[i]] = list2[i]

yyy = dc2 | dc3

# print(yyy)

ax = (ax
      .query("County in @yyy.keys()")
      .assign(County=lambda x: x["County"].replace(yyy)))

# ----------------------------------------------------------------
# merge
# ----------------------------------------------------------------

df = pd.DataFrame(df)
ax = pd.DataFrame(ax)
res = pd.merge(df, ax, on=['County','deliverymethod','city'], how='left')

# print(res)
res.to_excel('./res06.xlsx')

psh=[]

#галерея спб
# psh.append(res.loc[(res['deliverymethod'] == 'Курьер') & (res['County'] == 'Санкт-Петербург') & (res['stock'] == 155) & (res['INVENTLOCATIONID'] == '155')])
#спб им
# psh.append(res.loc[(res['deliverymethod'] == 'Курьер') & (res['County'] == 'Санкт-Петербург') & (res['stock'] == 223) & (res['INVENTLOCATIONID'] == '223')])
#курьер-мск-222
# psh.append(res.loc[(res['deliverymethod'] == 'Курьер') & (res['County' ] == 'Москва')  &  (res['stock' ] == 222)] & (res['Distict' ] == 'Зюзино'))
# psh.append(res.loc[(res['deliverymethod'] == 'Курьер') & (res['County' ] == 'Москва')  &  (res['stock' ] == 222)] & (res['Distict' ] == 'Котловка'))
# psh.append(res.loc[(res['deliverymethod'] == 'Курьер') & (res['County' ] == 'Москва')  &  (res['stock' ] == 222)] & (res['Distict' ] == 'Чертаново Северное'))
# psh.append(res.loc[(res['deliverymethod'] == 'Курьер') & (res['County' ] == 'Москва')  &  (res['stock' ] == 222)] & (res['Distict' ] == 'Чертаново Южное'))
# psh.append(res.loc[(res['deliverymethod'] == 'Курьер') & (res['County' ] == 'Москва')  &  (res['stock' ] == 222)] & (res['Distict' ] == 'Чертаново Центральное'))
# psh.append(res.loc[(res['deliverymethod'] == 'Курьер') & (res['County' ] == 'Москва')  &  (res['stock' ] == 222)] & (res['Distict' ] == 'Нагорный'))
# psh.append(res.loc[(res['deliverymethod'] == 'Курьер') & (res['County' ] == 'Москва')  &  (res['stock' ] == 222)] & (res['Distict' ] == 'Нагатино-Садовники'))
# psh.append(res.loc[(res['deliverymethod'] == 'Курьер') & (res['County' ] == 'Москва')  &  (res['stock' ] == 222)] & (res['Distict' ] == 'Нагатинский Затон'))
# psh.append(res.loc[(res['deliverymethod'] == 'Курьер') & (res['County' ] == 'Москва')  &  (res['stock' ] == 222)] & (res['Distict' ] == 'Бирюлево Западное'))
# psh.append(res.loc[(res['deliverymethod'] == 'Курьер') & (res['County' ] == 'Москва')  &  (res['stock' ] == 222)] & (res['Distict' ] == 'Бирюлево Восточное'))
# psh.append(res.loc[(res['deliverymethod'] == 'Курьер') & (res['County' ] == 'Москва')  &  (res['stock' ] == 222)] & (res['Distict' ] == 'Царицыно'))
# psh.append(res.loc[(res['deliverymethod'] == 'Курьер') & (res['County' ] == 'Москва')  &  (res['stock' ] == 222)] & (res['Distict' ] == 'Москворечье-Сабурово'))
# psh.append(res.loc[(res['deliverymethod'] == 'Курьер') & (res['County' ] == 'Москва')  &  (res['stock' ] == 222)] & (res['Distict' ] == 'Зябликово'))
# psh.append(res.loc[(res['deliverymethod'] == 'Курьер') & (res['County' ] == 'Москва')  &  (res['stock' ] == 222)] & (res['Distict' ] == 'Братеево'))
# psh.append(res.loc[(res['deliverymethod'] == 'Курьер') & (res['County' ] == 'Москва')  &  (res['stock' ] == 222)] & (res['Distict' ] == 'Орехово-Борисово Южное'))
# psh.append(res.loc[(res['deliverymethod'] == 'Курьер') & (res['County' ] == 'Москва')  &  (res['stock' ] == 222)] & (res['Distict' ] == 'Орехово-Борисово Северное'))
# psh.append(res.loc[(res['deliverymethod'] == 'Курьер') & (res['County' ] == 'Москва')  &  (res['stock' ] == 222)] & (res['Distict' ] == 'Капотня'))
# у всех позиций вставить инвентлокейшнайди 222

#курьер-мск-сток 222
# psh.append(res.loc[(res['deliverymethod'] == 'Курьер') & (res['County' ] == 'Москва')  &  (res['stock' ] == 222) & (res['INVENTLOCATIONID'] != '222') & (res['District'].isnull())])

# psh = pd.DataFrame(psh)

# print(psh)

# uni = []

# for item in psh['SalesId']: 
#     if item not in uni: 
#         uni.append(item)

# print(uni)

# # ----------------------------------------------------------------
# # получение списка уникальных SALESID
# # ----------------------------------------------------------------

# def unic(param):
#   dataSalesId = []
#   for line in param:
#     dataSalesId.append(line[3])

#   return set(dataSalesId)

# d

# # ----------------------------------------------------------------
# # создание нового списка с первыми строками каждого SALESID
# # ----------------------------------------------------------------

# def sortedData(params = []):
#   sortedListData = []
#   for ID in params[0]:
#     success = 0

#     for line in params[1]:
#       if ( line[3] == ID and success == 0 ):

#         success = 1
#         sortedListData.append(line)

#   return sortedListData


# # ----------------------------------------------------------------
# # формирование нового датафрейма pandas
# # ----------------------------------------------------------------

# def main(data):

#   listData = data.values.tolist()
#   listDataNew = []

#   listDataNew = pd.DataFrame(sortedData([ unic(listData), listData ]))
#   print(listDataNew)

# data = main(res)

# print(data)